module.exports.noOfMatchesPlayeredPerSeasonYear =
    function noOfMatchesPlayeredPerSeasonYear(matches) {
        try {

            let result = matches.reduce((acc, match) => {
                acc[match.season] = (acc[match.season] || 0) + 1;
                return acc;
            }, {})
            return result;

        } catch (exception) {
            console.log(exception.message);
        }
    }