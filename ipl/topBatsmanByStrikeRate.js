module.exports.topBatsmanByStrikeRate = function topBatsmanByStrikeRate(deliveries) {
    let strike = {};

    deliveries.map((delivery => {
        if (delivery.batsman in strike) {
            strike[delivery.batsman].ball += 1;
            strike[delivery.batsman].run += parseInt(delivery.batsman_runs);
        } else {
            strike[delivery.batsman] = {};
            strike[delivery.batsman].ball = 1;
            strike[delivery.batsman].run = parseInt(delivery.batsman_runs);
        }
    }));
    let result = {};
    for (let striker in strike) {
        result[striker] = (100 * (parseInt(strike[striker].run) / parseInt(strike[striker].ball)))
    }
    return result;

}