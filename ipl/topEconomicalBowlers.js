module.exports.topEconomicalBowlers = function topEconomicalBowlers(deliveries, matches) {
    const errMessage = "No data found";

    try {
        let idOf2015Matches = matches.filter((checkForYear) => {
            if (checkForYear.season == '2015') {
                return checkForYear;
            }
        }).map((checkId) => {
            return checkId.id;
        })
        let detailsOf2015Matches = deliveries.filter((delivery) => {
            if (idOf2015Matches.includes(delivery.match_id))
                return delivery;
        })
        let noOfBowls = detailsOf2015Matches.reduce((acc, current) => {
            if (current.wide_runs != '0' || current.noball_runs != '0') {
                return acc;
            } else {
                acc[current.bowler] = (acc[current.bowler] || 0) + 1;
                return acc;
            }
        }, {});
        let noOfRuns = detailsOf2015Matches.reduce((acc, current) => {
            acc[current.bowler] = (acc[current.bowler] || 0) + Number(current.total_runs) - Number(current.bye_runs) - Number(current.legbye_runs);
            return acc;
        }, {});
        var totalBallsByBowler = Object.values(noOfBowls)
        var bowlerName = Object.keys(noOfBowls);

        var totalRunsGivenByBowler = Object.values(noOfRuns)
        economy = totalBallsByBowler.map(function(bowls, index) {
            return { "name": bowlerName[index], "economy": (totalRunsGivenByBowler[index] * 6) / bowls };
        });
        let sortedEconomyRate = economy.sort(function(a, b) {
            return a.economy - b.economy;
        })
        let topTenEconomicalBowlerArray = sortedEconomyRate.slice(0, 10);
        var topTenEconomicalBowlerObject = topTenEconomicalBowlerArray.map(item => ({
            [item.name]: item.economy
        }));
        var topTenEconomicalBowler = Object.assign({}, ...topTenEconomicalBowlerObject);
        let topTenEconomicalBowlerStatus = { "bowlers": Object.keys(topTenEconomicalBowler), "economy": Object.values(topTenEconomicalBowler) };
        return topTenEconomicalBowlerStatus;
    } catch (exception) {
        console.log(exception.message)
    }
}