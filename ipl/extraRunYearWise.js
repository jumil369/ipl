const fs = require('fs')
module.exports.extraRunYearWise = function extraRunYearWise(deliveries, matches) {
    let result = {};
    let teamList = [];
    let idList2016 = [];
    const errMessage = "No data found";
    try {

        let result = deliveries.reduce((acc, delivery) => {
            let match = matches.map((match) => {
                if (match.id == delivery.match_id && match.season == '2016') {
                    if (delivery.bowling_team in acc) {
                        acc[delivery.bowling_team] += parseInt(delivery.extra_runs);
                    } else {
                        acc[delivery.bowling_team] = parseInt(delivery.extra_runs);
                    }
                }
            });
            return acc;
        }, {});
        return result;


    } catch (exception) {
        // console.log(exception.message)
    }

}