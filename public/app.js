fetchAndVisualizeData();

function fetchAndVisualizeData() {
    fetch('./data.json')
        .then(r => r.json())
        .then(data => {
            console.log(data)
            visualizeData(data);

        })

}

function visualizeData(data) {
    // No. of Matches Playered.

    var matchesPlayed = Highcharts.chart('matchesPlayed', {

        title: {
            text: 'No. of matches playered per season.'
        },

        subtitle: {
            text: 'IPL'
        },

        xAxis: {
            categories: Object.keys(data[0].noOfMatchesPlayeredPerSeasonYear)
        },

        series: [{
            type: 'column',
            name: 'matches',
            colorByPoint: true,
            data: Object.values(data[0].noOfMatchesPlayeredPerSeasonYear),
            showInLegend: false
        }]

    });

    // No. of matches won per team

    Highcharts.chart('wonByTeam', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches Won by Team per Year'
        },
        xAxis: {
            title: {
                text: 'Years'
            },
            categories: Object.keys(data[1].noOfMatchesWonPerTeam.Years[0]),
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Teams'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: false,
            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 0,
            shadow: true
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        },
        series: [{
                name: data[1].noOfMatchesWonPerTeam.Teams[0],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[0])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[1],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[1])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[2],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[2])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[3],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[3])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[4],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[4])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[5],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[5])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[6],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[6])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[7],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[7])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[8],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[8])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[9],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[9])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[10],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[10])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[11],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[11])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[12],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[12])
            },
            {
                name: data[1].noOfMatchesWonPerTeam.Teams[13],
                data: Object.values(data[1].noOfMatchesWonPerTeam.Years[13])
            },
        ]
    });

    // Extra runs year wise

    var extraRuns = Highcharts.chart('extraRuns', {

        title: {
            text: 'Extra Runs Year Wise Per Team'
        },

        subtitle: {
            text: 'in IPL 2016'
        },

        xAxis: {
            categories: Object.keys(data[2].extraRunYearWise)
        },

        series: [{
            type: 'column',
            name: 'Extra Runs',
            colorByPoint: true,
            data: Object.values(data[2].extraRunYearWise),
            showInLegend: false
        }]

    });

    // Top economic bowlers

    var topBolwer = Highcharts.chart('topBolwer', {

        title: {
            text: 'To Ten Bolwers'
        },

        subtitle: {
            text: 'in IPL 2015'
        },

        xAxis: {
            categories: Object.values(data[3].topEconomicalBowlers.bowlers)
        },

        series: [{
            type: 'column',
            name: 'Economy',
            colorByPoint: true,
            data: Object.values(data[3].topEconomicalBowlers.economy),
            showInLegend: false
        }]

    });
}