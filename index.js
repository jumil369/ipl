const CSVToJSON = require("csvtojson");
const fs = require('fs')

const noOfMatchesPlayeredPerSeasonYear = require('./ipl/noOfMatchesPlayeredPerSeasonYear.js');
const noOfMatchesWonPerTeam = require('./ipl/noOfMatchesWonPerTeam.js');
const extraRunYearWise = require('./ipl/extraRunYearWise.js');
const topBatsmanByStrikeRate = require('./ipl/topBatsmanByStrikeRate.js')
const topEconomicalBowlers = require('./ipl/topEconomicalBowlers.js');



writeDataMatchesCSVToJson();
writeDataDeliveriesCSVToJson();
ipl();

function writeDataMatchesCSVToJson() {
    CSVToJSON().fromFile("./csv_files/matches.csv").then(matches => {
        const jsonString = JSON.stringify(matches)
        fs.writeFile('./matches.json', jsonString, err => {
            if (err) {
                console.log('Error writing file', err)
            } else {
                console.log('Successfully wrote matches.json')
            }
        })
    });
}

function writeDataDeliveriesCSVToJson() {
    CSVToJSON().fromFile("./csv_files/deliveries.csv").then(deliveries => {
        const jsonString = JSON.stringify(deliveries)
        fs.writeFile('./deliveries.json', jsonString, err => {
            if (err) {
                console.log('Error writing file', err)
            } else {
                console.log('Successfully wrote deliveries.json')
            }
        })
    });
}

function ipl() {
    let dataObject = [];
    try {
        const matches = require('./matches.json');
        const deliveries = require('./deliveries.json');
        topBatsmanByStrikeRate.topBatsmanByStrikeRate(deliveries);
        // dataObject.push({ "noOfMatchesPlayeredPerSeasonYear": noOfMatchesPlayeredPerSeasonYear.noOfMatchesPlayeredPerSeasonYear(matches) });
        // dataObject.push({ "noOfMatchesWonPerTeam": noOfMatchesWonPerTeam.noOfMatchesWonPerTeam(matches) });
        // dataObject.push({ "extraRunYearWise": extraRunYearWise.extraRunYearWise(deliveries, matches) });
        // dataObject.push({ "topEconomicalBowlers": topEconomicalBowlers.topEconomicalBowlers(deliveries, matches) });
        // const jsonString = JSON.stringify(dataObject)
        // fs.writeFile('./public/data.json', jsonString, err => {
        //     if (err) {
        //         console.log('Error writing file', err)
        //     } else {
        //         console.log('Successfully wrote file')
        //     }
        // })

    } catch (exception) {
        console.log(exception.message);
    }
}