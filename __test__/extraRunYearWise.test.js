const { extraRunYearWise } = require('../ipl/extraRunYearWise');



let matchs = [{
        id: '45',
        season: '2016',
        date: '2017-05-07',
        team1: 'Royal Challengers Bangalore',
        team2: 'Kolkata Knight Riders',

    },
    {
        id: '46',
        season: '2016',
        city: 'Chandigarh',
        date: '2017-05-07',
        team1: 'Kings XI Punjab',
        team2: 'Gujarat Lions',

    },
    {
        id: '47',
        season: '2016',
        city: 'Hyderabad',
        date: '2017-05-08',
        team1: 'Mumbai Indians',
        team2: 'Sunrisers Hyderabad',
    },
    {
        id: '48',
        season: '2016',
        city: 'Chandigarh',
        date: '2017-05-09',
        team1: 'Kings XI Punjab',
        team2: 'Kolkata Knight Riders',
    },
    {
        id: '49',
        season: '2015',
        city: 'Kanpur',
        date: '2017-05-10',
        team1: 'Gujarat Lions',
        team2: 'Delhi Daredevils',
    },
    {
        id: '50',
        season: '2015',
        city: 'Mumbai',
        date: '2017-05-11',
        team1: 'Kings XI Punjab',
        team2: 'Mumbai Indians',
    }
];

let delivery = [{
        match_id: '45',
        bowling_team: 'Royal Challengers Bangalore',
        batsman_runs: '10',
        extra_runs: '10',
        total_runs: '20',
    },
    {
        match_id: '46',
        bowling_team: 'Gujarat Lions',
        bowler: 'YS Chahal',
        extra_runs: '5',
        total_runs: '41',
    },
    {
        match_id: '47',
        inning: '1',
        bowling_team: 'Sunrisers Hyderabad',
        bowler: 'YS Chahal',
        extra_runs: '10',
        total_runs: '30',
    },
    {
        match_id: '48',
        inning: '1',
        bowling_team: 'Kolkata Knight Riders',
        bowler: 'YS Chahal',
        extra_runs: '20',
        total_runs: '21',
    },
    {
        match_id: '49',
        inning: '1',
        bowling_team: 'Delhi Daredevils',
        bowler: 'YS Chahal',
        extra_runs: '3',
        total_runs: '4',
    },
    {
        match_id: '50',
        inning: '1',
        bowling_team: 'Mumbai Indians',
        bowler: 'TS Mills',
        extra_runs: '0',
        total_runs: '1',
    }
]

test("Extra runs year wise Test Case 1", () => {
    expect(extraRunYearWise(delivery, matchs)).toEqual({ "Gujarat Lions": 5, "Kolkata Knight Riders": 20, "Royal Challengers Bangalore": 10, "Sunrisers Hyderabad": 10 })
});

test("Extra runs year wise Test Case 2", () => {
    expect(extraRunYearWise([], [])).toEqual({})
});

test("Extra runs year wise Test Case 3", () => {
    expect(extraRunYearWise([])).toEqual({})
});

test("Extra runs year wise Test Case 4", () => {
    expect(extraRunYearWise([delivery], [])).toEqual({})
});

test("Extra runs year wise Test Case 5", () => {
    expect(extraRunYearWise([], [matchs])).toEqual({})
});