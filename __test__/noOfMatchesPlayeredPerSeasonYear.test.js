const { noOfMatchesPlayeredPerSeasonYear } = require('../ipl/noOfMatchesPlayeredPerSeasonYear');


let matchs = [{
        id: '45',
        season: '2017',
        city: 'Bangalore',
        date: '2017-05-07',
        team1: 'Royal Challengers Bangalore',
        team2: 'Kolkata Knight Riders',
        toss_winner: 'Kolkata Knight Riders',
        toss_decision: 'field',
        result: 'normal',
        dl_applied: '0',
        winner: 'Kolkata Knight Riders',
        win_by_runs: '0',
        win_by_wickets: '6',
        player_of_match: 'SP Narine',
        venue: 'M Chinnaswamy Stadium',
        umpire1: 'AY Dandekar',
        umpire2: 'C Shamshuddin',
        umpire3: ''
    },
    {
        id: '46',
        season: '2016',
        city: 'Chandigarh',
        date: '2017-05-07',
        team1: 'Kings XI Punjab',
        team2: 'Gujarat Lions',
        toss_winner: 'Gujarat Lions',
        toss_decision: 'field',
        result: 'normal',
        dl_applied: '0',
        winner: 'Gujarat Lions',
        win_by_runs: '0',
        win_by_wickets: '6',
        player_of_match: 'DR Smith',
        venue: 'Punjab Cricket Association IS Bindra Stadium, Mohali',
        umpire1: 'A Nand Kishore',
        umpire2: 'VK Sharma',
        umpire3: ''
    },
    {
        id: '47',
        season: '2016',
        city: 'Hyderabad',
        date: '2017-05-08',
        team1: 'Mumbai Indians',
        team2: 'Sunrisers Hyderabad',
        toss_winner: 'Mumbai Indians',
        toss_decision: 'bat',
        result: 'normal',
        dl_applied: '0',
        winner: 'Sunrisers Hyderabad',
        win_by_runs: '0',
        win_by_wickets: '7',
        player_of_match: 'S Dhawan',
        venue: 'Rajiv Gandhi International Stadium, Uppal',
        umpire1: 'KN Ananthapadmanabhan',
        umpire2: 'M Erasmus',
        umpire3: ''
    },
    {
        id: '48',
        season: '2015',
        city: 'Chandigarh',
        date: '2017-05-09',
        team1: 'Kings XI Punjab',
        team2: 'Kolkata Knight Riders',
        toss_winner: 'Kolkata Knight Riders',
        toss_decision: 'field',
        result: 'normal',
        dl_applied: '0',
        winner: 'Kings XI Punjab',
        win_by_runs: '14',
        win_by_wickets: '0',
        player_of_match: 'MM Sharma',
        venue: 'Punjab Cricket Association IS Bindra Stadium, Mohali',
        umpire1: 'A Nand Kishore',
        umpire2: 'S Ravi',
        umpire3: ''
    },
    {
        id: '49',
        season: '2015',
        city: 'Kanpur',
        date: '2017-05-10',
        team1: 'Gujarat Lions',
        team2: 'Delhi Daredevils',
        toss_winner: 'Delhi Daredevils',
        toss_decision: 'field',
        result: 'normal',
        dl_applied: '0',
        winner: 'Delhi Daredevils',
        win_by_runs: '0',
        win_by_wickets: '2',
        player_of_match: 'SS Iyer',
        venue: 'Green Park',
        umpire1: 'YC Barde',
        umpire2: 'AK Chaudhary',
        umpire3: ''
    },
    {
        id: '50',
        season: '2015',
        city: 'Mumbai',
        date: '2017-05-11',
        team1: 'Kings XI Punjab',
        team2: 'Mumbai Indians',
        toss_winner: 'Mumbai Indians',
        toss_decision: 'field',
        result: 'normal',
        dl_applied: '0',
        winner: 'Kings XI Punjab',
        win_by_runs: '7',
        win_by_wickets: '0',
        player_of_match: 'WP Saha',
        venue: 'Wankhede Stadium',
        umpire1: 'A Deshmukh',
        umpire2: 'A Nand Kishore',
        umpire3: ''
    }
];




test("Number of Matches Played Per Season Test Case 1", () => {
    expect(noOfMatchesPlayeredPerSeasonYear(matchs)).toEqual({ '2015': 3, '2016': 2, '2017': 1 });
})

test("Number of Matches Played Per Season Test Case 2", () => {
    expect(noOfMatchesPlayeredPerSeasonYear([])).toEqual({});
});