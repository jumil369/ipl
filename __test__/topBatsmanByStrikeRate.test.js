const { topBatsmanByStrikeRate } = require('../ipl/topBatsmanByStrikeRate.js');
let delivery = [{
        batsman: 'Yuvraj Singh',
        batsman_runs: '5',
    },
    {
        batsman: 'Yuvraj Singh',
        batsman_runs: '1',
    },
    {
        batsman: 'Yuvraj Singh',
        batsman_runs: '0',
    },
    {
        batsman: 'DJ Hooda',
        batsman_runs: '1',
    },
    {
        batsman: 'DJ Hooda',
        batsman_runs: '1',
    }
]


test("Top 10 batsman by strike rate Test Case 1", () => {
    expect(topBatsmanByStrikeRate(delivery)).toEqual({
        'Yuvraj Singh': 200,
        'DJ Hooda': 100
    })
});